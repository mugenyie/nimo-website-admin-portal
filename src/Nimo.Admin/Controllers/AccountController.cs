﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nimo.Admin.Models;

namespace Nimo.Admin.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(AdminLoginViewModel AdminLogin)
        {
            return RedirectToAction("Index", "Admin");
        }

        public IActionResult Register()
        {
            return View();
        }
    }
}