﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nimo.Admin.Models
{
    public class AdminLoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
