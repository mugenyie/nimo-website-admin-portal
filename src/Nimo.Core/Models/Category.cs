﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string CategoryProducts { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string PictureUrl { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsPublished { get; set; }
        public List<string> ChildCategories { get; set; }
        public string ParentCategoryId { get; set; }
        public DateTime DeletedOn { get; set; }
    }
}
