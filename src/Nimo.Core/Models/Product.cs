﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public bool IsFeatured { get; set; }
        public int ReviewsCount { get; set; }
        public int RatingAverage { get; set; }
        public string PictureUrl { get; set; }
        public string CategoryId { get; set; }
        public string VendorId { get; set; }
        public List<ProductReview> ProductReviews { get; set; }
        public List<string> ProductCategories { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOnUtc { get; set; }
    }
}
