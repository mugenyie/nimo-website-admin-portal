﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Models.Requests
{
    public class AddCategoryRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string PictureUrl { get; set; }
        public string ParentCategoryId { get; set; }
    }
}
