﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Models.Responses
{
    public class CategoriesResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public List<Category> Categories { get; set; }
    }
}
