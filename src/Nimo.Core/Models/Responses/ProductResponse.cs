﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Models.Responses
{
    public class ProductResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public Product Product { get; set; }
    }
}
