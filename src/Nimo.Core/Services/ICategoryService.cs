﻿using Nimo.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Services
{
    public interface ICategoryService
    {
        List<Category> GetCategories();
        Category AddCategory(Category Category);
    }
}
