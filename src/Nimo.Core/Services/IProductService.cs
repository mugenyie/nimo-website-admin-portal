﻿using Nimo.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Core.Services
{
    public interface IProductService
    {
        Product GetProduct(string ProductId);
        List<Product> GetProducts();
        Product AddProduct(Product Product);
    }
}
