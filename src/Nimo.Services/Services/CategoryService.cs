﻿using Nimo.Core.Models;
using Nimo.Core.Models.Responses;
using Nimo.Core.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private IRestClient _restClient;
        private IRestRequest _restRequest;

        public CategoryService(string BaseUrl)
        {
            _restRequest = new RestRequest(BaseUrl);
        }

        public Category AddCategory(Category Category)
        {
            _restRequest = new RestRequest(Method.POST)
            {
                Resource = $"/admin/api/Categories"
            };

            _restRequest.AddJsonBody(Category);

            var response = _restClient.Execute<CategoryResponse>(_restRequest);

            return response.Data.Category;
        }

        public List<Category> GetCategories()
        {
            _restRequest = new RestRequest(Method.GET)
            {
                Resource = $"/api/Categories"
            };

            var response = _restClient.Execute<CategoriesResponse>(_restRequest);

            return response.Data.Categories;
        }
    }
}
