﻿using Nimo.Core.Models;
using Nimo.Core.Models.Responses;
using Nimo.Core.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimo.Services.Services
{
    public class ProductService : IProductService
    {
        private IRestClient _restClient;
        private IRestRequest _restRequest;

        public ProductService(string BaseUrl)
        {
            _restClient = new RestClient(BaseUrl);
        }

        public Product AddProduct(Product Product)
        {
            _restRequest = new RestRequest(Method.POST)
            {
                Resource = $"/admin/api/Products"
            };

            _restRequest.AddJsonBody(Product);

            var response = _restClient.Execute<ProductResponse>(_restRequest);

            return response.Data.Product;
        }

        public Product GetProduct(string ProductId)
        {
            _restRequest = new RestRequest(Method.GET)
            {
                Resource = $"/api/Products/{ProductId}"
            };

            var response = _restClient.Execute<ProductResponse>(_restRequest);

            return response.Data.Product;
        }

        public List<Product> GetProducts()
        {
            _restRequest = new RestRequest(Method.GET)
            {
                Resource = $"/api/Products"
            };

            var response = _restClient.Execute<ProductsResponse>(_restRequest);

            return response.Data.Products;
        }
    }
}
